from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.db import connection

# Create your views here.
def index(request):
    html = 'daftarbeasiswa.html'
    return render(request, html)

def daftar(request):
    kode = request.POST['kode']
    npm = request.POST['npm']
    email = request.POST['email']
    ips = request.POST['ips']
    cursor=connection.cursor()
    values = kode+", '"+npm+"', CURRENT_TIMESTAMP, 'Aktif', 'Belum Diterima'"
    query = "INSERT INTO pendaftaran values (" + values + ")"
    cursor.execute(query)
    return HttpResponseRedirect(reverse('home:index_all'))
