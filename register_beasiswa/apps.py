from django.apps import AppConfig


class RegisterBeasiswaConfig(AppConfig):
    name = 'register_beasiswa'
