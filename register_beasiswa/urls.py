from django.conf.urls import url
from .views import index, daftar


#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^daftar/', daftar, name='daftar'),
]