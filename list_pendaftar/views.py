from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
    html='terimabeasiswa.html'
    return render(request,html)

def status(request):
    html='statusbeasiswa.html'
    return render(request,html)