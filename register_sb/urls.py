from django.conf.urls import url
from .views import index, tambah_paket_beasiswa

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^tambahpaket/', tambah_paket_beasiswa, name='tambah_paket_beasiswa'),
]