from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.db import connection

response = {}
nomor_identitas_donatur = '444-78-5633'

# Create your views here.
def index(request):
    html = "sb/daftarpaketbeasiswa.html"
    return render(request, html, response)

def tambah_paket_beasiswa(request):
    if request.method == 'POST':
        kode = request.POST['kode']
        nama = request.POST['nama']
        jenis = request.POST['jenis']
        deskripsi = request.POST['deskripsi']
        syarat = request.POST['syarat']

        list_skema_beasiswa = [kode, nama, jenis, deskripsi, nomor_identitas_donatur]

        cursor = connection.cursor()
        query1 = query_skema_beasiswa(list_skema_beasiswa)
        query2 = query_syarat_beasiswa(kode, syarat)

        cursor.execute(query1)
        for query in query2:
            cursor.execute(query)
        cursor.close()

        return HttpResponseRedirect(reverse('home:index_donatur'))

def query_skema_beasiswa(list):
    res = "INSERT INTO SKEMA_BEASISWA (kode,nama,jenis,deskripsi,nomor_identitas_donatur) VALUES ('%s', '%s', '%s', '%s', '%s')" % (list[0], list[1], list[2], list[3], list[4])
    return res

def query_syarat_beasiswa(kode, syarat):
    res = []
    list_syarat = syarat.split(";")

    for data in list_syarat:
        res.append("INSERT INTO SYARAT_BEASISWA (kode_beasiswa, syarat) VALUES ('%s', '%s')" % (kode, data))
    
    return res