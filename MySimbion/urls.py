"""MySimbion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView

import home_all.urls as home_all
import list_pendaftar.urls as list_pendaftar
import login.urls as login
import register_akun.urls as register_akun
import register_beasiswa.urls as register_beasiswa
import register_sb.urls as register_sb
import register_sbaktif.urls as register_sbaktif

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/login/', permanent=True), name = 'index'),

    #rafif
    url(r'^login/', include(login,namespace='login')),
    url(r'^register_akun/', include(register_akun,namespace='register_akun')),

    #rehan
    url(r'^register_sb/', include(register_sb,namespace='register_sb')),
    url(r'^register_sbaktif/', include(register_sbaktif,namespace='register_sbaktif')),
    url(r'^home/', include(home_all,namespace='home')),

    #prima
    url(r'^list_pendaftar/', include(list_pendaftar,namespace='list_pendaftar')),
    url(r'^register_beasiswa/', include(register_beasiswa,namespace='register_beasiswa'))
]
