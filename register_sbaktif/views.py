from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.db import connection

username_donatur = 'bcade68'

# Create your views here.
def index(request):
    html = "sbaktif/tambahbeasiswa.html"

    cursor = connection.cursor()
    cursor.execute("SELECT kode FROM SKEMA_BEASISWA S, DONATUR D WHERE S.nomor_identitas_donatur=D.nomor_identitas AND D.username='" + username_donatur + "' ORDER BY kode ASC")

    return render(request, html, {'kode_be': cursor.fetchall})

def tambah_sba(request):

    if request.method == 'POST':

        kode = request.POST['kode']
        nourut = request.POST['nourut']
        mulai = request.POST['tglmulai']
        tutup = request.POST['tgltutup']
        # mulai = datetime.strptime(request.POST['tglmulai'], '%Y-%m-%d').strftime('%m/%d/%y')
        # tutup = datetime.strptime(request.POST['tgltutup'], '%Y-%m-%d').strftime('%m/%d/%y')
        periode = 'January-December'
        status = 'dibuka'
        jumlah_pendaftar = 0

        data_in_list = [kode, nourut, mulai, tutup, periode, status, jumlah_pendaftar]

        cursor = connection.cursor()
        # query = "INSERT INTO SKEMA_BEASISWA_AKTIF (kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status,jumlah_pendaftar) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" %\
        # (kode, nourut, mulai, tutup, periode, status, jumlah_pendaftar)
        query = query_maker(data_in_list)
        
        cursor.execute(query)
        cursor.close()

        return HttpResponseRedirect(reverse('home:index_donatur'))

def query_maker(list):
    res = "INSERT INTO SKEMA_BEASISWA_AKTIF (kode_skema_beasiswa,no_urut,tgl_mulai_pendaftaran,tgl_tutup_pendaftaran,periode_penerimaan,status,jumlah_pendaftar) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')" %\
    (list[0], list[1], list[2], list[3], list[4], list[5], list[6])
    return res
