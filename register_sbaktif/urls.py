from django.conf.urls import url
from .views import index, tambah_sba

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^tambah_baru/', tambah_sba, name='tambah'),
]