from django.shortcuts import render
from django.http import HttpResponse
from django.db import connection

# Create your views here.
def index_all(request):
    response = {}
    html = "list_beasiswa_all_user.html"

    #query semua skema beasiswa aktif buat ditampilin di dashboard yang semua orang bisa liat
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM SKEMA_BEASISWA_AKTIF ORDER BY (kode_skema_beasiswa,no_urut) ASC")
    response['beasiswa'] = cursor.fetchall()
    cursor.close()

    return render(request, html, response)

def index_donatur(request):

    # dummy account, nunggu fitur login logout dulu
    username_donatur = 'bcade68'
    nomor_identitas_donatur = '444-78-5633'

    response = {}
    html = "lihatbeasiswa_donatur.html"

    #query semua skema beasiswa aktif yang dibuat sama orang yang lagi login
    cursor1 = connection.cursor()
    cursor1.execute("SELECT * FROM SKEMA_BEASISWA_AKTIF SA, SKEMA_BEASISWA S, DONATUR D WHERE SA.kode_skema_beasiswa=S.kode AND D.nomor_identitas=S.nomor_identitas_donatur AND D.username='" + username_donatur + "' ORDER BY (kode_skema_beasiswa,no_urut) ASC")
    response['beasiswa'] = cursor1.fetchall()
    cursor1.close()
    
    #query semua skema beasiswa (paket) yang dibuat sama orang yang lagi login
    cursor2 = connection.cursor()
    cursor2.execute("SELECT kode, nama, count(no_urut) FROM SKEMA_BEASISWA S LEFT JOIN SKEMA_BEASISWA_AKTIF SA ON S.kode=SA.kode_skema_beasiswa where S.nomor_identitas_donatur='%s' GROUP BY (kode,nama)" % nomor_identitas_donatur)
    response['skema'] = cursor2.fetchall()
    cursor2.close()
    
    return render(request, html, response)
    
def deskripsi_beasiswa(request, kode):
    response = {}
    html = "deskripsibeasiswa.html"

    c = connection.cursor()
    c.execute("SELECT * FROM SKEMA_BEASISWA WHERE kode=%s" % kode)
    response['info'] = c.fetchall()

    return render(request, html, response)