from django.conf.urls import url
from django.views.generic.base import RedirectView
from .views import index_all, index_donatur, deskripsi_beasiswa

#url for app
urlpatterns = [
    url(r'^$', index_all, name='index_all'),
    url(r'^donatur/', index_donatur, name='index_donatur'),
    url(r'^deskripsi/(?P<kode>\w+)/$', deskripsi_beasiswa, name='deskripsi_beasiswa'),
]